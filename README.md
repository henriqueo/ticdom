# Clustering of respondents based on Internet usage habits

Based on a subset extracted from data made available in 2020 (the research was conducted in 2019) by [Cetic.br](https://cetic.br/pt/) about the habits of Brazilian Internet users, 4 profiles could be identified, by applying the well-know K-Means algorithm. The original dataset can be found [here](https://cetic.br/pt/arquivos/domicilios/2019/domicilios/).

## API usage

Service endpoint

- `https://ticdom.herokuapp.com`

### REST Resource: /user
|Method||
|--- | --- |
|create|`POST /user` <br> Creates a new user registry and fits it into one of the four profiles.|
|read |`GET /user/{id}` <br> Reads a user's quest number and cluster.|

#### HTTP Request

`POST https://ticdom.herokuapp.com/user`

```bash
curl -X POST https://ticdom.herokuapp.com/user \
    -H 'Content-Type: application/json' \
        -d '{"quest": 2,
        "c7_a": false,
        "c7_b": true,
        "c7_c": true,
        "c7_d": true,
        "c7_e": true,
        "c7_f": true,
        "c8_a": true,
        "c8_b": true,
        "c8_c": true,
        "c8_d": true,
        "c8_e": true,
        "c8_f": true,
        "c8_g": true,
        "c8_h": true,
        "c9_a": true,
        "c9_b": true,
        "c9_c": true,
        "c9_d": true,
        "c9_f": true,
        "c9_g": true,
        "c10_a": false,
        "c10_b": false,
        "c10_c": false,
        "c10_d": false,
        "c10_e": false,
        "c10_f": false,
        "c11_a": false,
        "c11_b": false,
        "c11_c": false,
        "c12_a": false,
        "c12_b": false,
        "c12_c": false,
        "c12_d": false,
        "c12_e1": true,
        "c12_f1": true
        }'
```
### Fields

|Field|Type|Description|
|--- | --- | ---|
|quest|Integer|Número de identificação do questionário (unique)|
|c7_a|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para enviar e receber e-mail?|
|c7_b|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para enviar mensagens instantâneas (como, por exemplo,  por Facebook, Skype e Whatsapp)?|
|c7_c|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para conversar por voz ou vídeo através de programas como Skype ou no Whatsapp?|
|c7_d|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para participar de redes sociais, como Facebook, Instagram ou Snapchat?|
|c7_e|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para participar de listas de discussão ou fóruns?|
|c7_f|Boolean|Nos últimos 3 meses,  o respondente utilizou a Internet para usar microblog como, por exemplo, Twitter?|
|c8_a|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar informações sobre produtos e serviços?|
|c8_b|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar informações relacionadas à saúde ou a serviços de saúde?|
|c8_c|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar informações sobre viagens e acomodações?|
|c8_d|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar emprego ou enviar currículos?|
|c8_e|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar informações em sites de enciclopédia virtual como Wikipédia?|
|c8_f|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para procurar informações oferecidas por sites de governo?|
|c8_g|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para realizar algum serviço público como, por exemplo, emitir documentos pela Internet, preencher e enviar formulários online, ou pagar taxas e impostos pela Internet?|
|c8_h|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para fazer consultas, pagamentos ou outras transações financeiras?|
|c9_a|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para jogar on-line?|
|c9_b|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para ouvir música on-line como por Spotify, por Deezer ou por Youtube?|
|c9_c|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para assistir vídeos, programas, filmes ou séries em sites como o Youtube ou Netflix?|
|c9_d|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para ler jornais, revistas ou notícias?|
|c9_f|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para ver exposições e museus?|
|c9_g|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para ouvir podcasts?|
|c10_a|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para realizar atividades ou pesquisas escolares?|
|c10_b|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para fazer cursos à distância?|
|c10_c|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para buscar informações sobre cursos de graduação, pós-graduação e de extensão?|
|c10_d|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para estudar na Internet por conta própria?|
|c10_e|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para usar serviço de armazenamento na Internet, como por exemplo Dropbox, Google Drive, Onedrive?|
|c10_f|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para realizar atividades de trabalho?|
|c11_a|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para compartilhar conteúdo na Internet, como textos, imagens, fotos, vídeos ou músicas?|
|c11_b|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para criar ou atualizar blogs, páginas na Internet ou websites?|
|c11_c|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para postar na Internet textos, imagens, fotos, vídeos ou músicas que o respondente mesmo fez?|
|c12_a|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de filmes?|
|c12_b|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de músicas?|
|c12_c|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de jogos?|
|c12_d|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de softwares, programas de computador ou aplicativos?|
|c12_e1|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de livros digitais?|
|c12_f1|Boolean|Nos últimos 3 meses, o respondente utilizou a Internet para baixar ou fazer o download de séries?|

#### Response

```json
{
    "$uri": "/user/2",
    "cluster": 2,
    "quest": 2
}
```

#### HTTP Request

`GET https://ticdom.herokuapp.com/user/2`

```bash
curl https://ticdom.herokuapp.com/user/2
```

#### Response

```json
{
    "$uri": "/user/2",
    "cluster": 2,
    "quest": 2
}
```


