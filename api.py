#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Henrique Oliveira <henriqueo@protonmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_potion import Api, ModelResource, Resource, fields, manager
import flask_potion.routes as routes

from cluster import predict_cluster


app = Flask(__name__)
app.debug = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///ticdom.db'
db = SQLAlchemy(app)
api = Api(app)

class User(db.Model):
    id      = db.Column(db.Integer, primary_key=True)
    quest   = db.Column(db.Integer, unique=True)
    cluster = db.Column(db.Integer)
    c7_a    = db.Column(db.Boolean)
    c7_b    = db.Column(db.Boolean)
    c7_c    = db.Column(db.Boolean)
    c7_d    = db.Column(db.Boolean)
    c7_e    = db.Column(db.Boolean)
    c7_f    = db.Column(db.Boolean)
    c8_a    = db.Column(db.Boolean)
    c8_b    = db.Column(db.Boolean)
    c8_c    = db.Column(db.Boolean)
    c8_d    = db.Column(db.Boolean)
    c8_e    = db.Column(db.Boolean)
    c8_f    = db.Column(db.Boolean)
    c8_g    = db.Column(db.Boolean)
    c8_h    = db.Column(db.Boolean)
    c9_a    = db.Column(db.Boolean)
    c9_b    = db.Column(db.Boolean)
    c9_c    = db.Column(db.Boolean)
    c9_d    = db.Column(db.Boolean)
    c9_f    = db.Column(db.Boolean)
    c9_g    = db.Column(db.Boolean)
    c10_a   = db.Column(db.Boolean)
    c10_b   = db.Column(db.Boolean)
    c10_c   = db.Column(db.Boolean)
    c10_d   = db.Column(db.Boolean)
    c10_e   = db.Column(db.Boolean)
    c10_f   = db.Column(db.Boolean)
    c11_a   = db.Column(db.Boolean)
    c11_b   = db.Column(db.Boolean)
    c11_c   = db.Column(db.Boolean)
    c12_a   = db.Column(db.Boolean)
    c12_b   = db.Column(db.Boolean)
    c12_c   = db.Column(db.Boolean)
    c12_d   = db.Column(db.Boolean)
    c12_e1  = db.Column(db.Boolean)
    c12_f1  = db.Column(db.Boolean)

db.create_all()

class UserResource(ModelResource):
    class Meta:
        name = 'user'
        model = User
        read_only_fields = ['cluster']
        write_only_fields = [
            'c7_a', 'c7_b', 'c7_c', 'c7_d', 'c7_e', 'c7_f', 'c8_a', 'c8_b',
            'c8_c', 'c8_d', 'c8_e', 'c8_f', 'c8_g', 'c8_h', 'c9_a', 'c9_b',
            'c9_c', 'c9_d', 'c9_f', 'c9_g', 'c10_a', 'c10_b', 'c10_c',
            'c10_d', 'c10_e', 'c10_f', 'c11_a', 'c11_b', 'c11_c', 'c12_a',
            'c12_b', 'c12_c', 'c12_d', 'c12_e1', 'c12_f1']

    class Schema:
        cluster = fields.Integer(minimum=0, maximum=3)

    @routes.Route.POST('',
        rel='create',
        schema=fields.Inline('self'),
        response_schema=fields.Inline('self'))
    def create(self, properties, *args, **kwargs):
        properties['cluster'] = predict_cluster(properties)
        return self.manager.create(properties)


api.add_resource(UserResource)

@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect('https://gitlab.com/henriqueo/ticdom')

if __name__ == '__main__':
    app.run(port=8080)

