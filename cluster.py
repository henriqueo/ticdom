#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Henrique Oliveira <henriqueo@protonmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
from joblib import load
import pickle
import numpy as np


clf = pickle.load(open('./models/ticdom-internet-usage-habits.sav', 'rb'))

def predict_cluster(user_habits):
    user = [int(v) for k,v in user_habits.items()][1:]

    user_array = np.array(user).reshape(1, -1)

    predict_cluster = clf.predict(user_array)[0]

    return int(predict_cluster)

